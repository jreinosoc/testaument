import express from "express";
import sales from "./routes/sales.router.js";

const app = express();
const port = 3000;

app.use(express.json());

// route sales
app.use("/", sales);

// 404
app.use("*", (req, res, next) => {
  res
    .status(404)
    .send(
      "404 Not Found - The page you are trying to find does not exist on this site"
    );
});

app.listen(port, () => {
  console.log(`Hello Aument 👋, the server is running in ➜ http://localhost:${port}`);
});

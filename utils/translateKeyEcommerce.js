const translateKey = (ecommerce, key) => {
  const commerces = {
    Woocommerce: {
      email: "customer.email",
      name: "customer.name",
      price: "money",
      payment_status: "payment-stats",
      status_payed: "completed",
    },
    Shopify: {
      email: "customer.email",
      name: "customer.name",
      price: "price",
      payment_status: "payment_status",
      status_payed: "payed",
    },
    newCommerceSpanish: {
      email: "cliente.correo",
      name: "cliente.nombre",
      price: "valor",
      payment_status: "estado_pago",
      status_payed: "pago",
    },
  };
  return commerces[ecommerce][key];
};
export default translateKey;

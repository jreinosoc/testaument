import express from "express";
import * as allData from "../data/ecommercesData.js";
import translateKey from "../utils/translateKeyEcommerce.js";
const router = express.Router();

const getTotalBiling = () => {
  let total = 0;

  // list of all Ecommerces
  Object.keys(allData).map((ecommerce) => {
    const translatePrice = translateKey(ecommerce, "price");
    const translatePaymentStatus = translateKey(ecommerce, "payment_status");
    const translateStatus_payed = translateKey(ecommerce, "status_payed");
    allData[ecommerce].map((data) => {
      const posValor = translatePrice.split(".");
      let valor = "";
      for (let index = 0; index < posValor.length; index++) {
        if (!valor) {
          valor = data[posValor[index]];
        } else {
          valor = valor[posValor[index]];
        }
      }
      if (data[translatePaymentStatus] === translateStatus_payed) {
        total += parseFloat(valor);
      }
    });
  });

  return total;
};

const getUniqueEmails = () => {
  let emails = [];
  // const AllSales = Shopify.concat(Woocommerce);
  Object.keys(allData).map((ecommerce) => {
    const translateEmail = translateKey(ecommerce, "email");
    allData[ecommerce].map((data) => {
      const posEmail = translateEmail.split(".");
      let email = "";
      for (let index = 0; index < posEmail.length; index++) {
        if (!email) {
          email = data[posEmail[index]];
        } else {
          email = email[posEmail[index]];
        }
      }
      if (!emails.includes(data.customer.email)) {
        emails.push(data.customer.email);
      }
    });
  });

  // AllSales.map((data) => {
  //   if (!emails.includes(data.customer.email)) {
  //     emails.push(data.customer.email);
  //   }
  // });
  return emails;
};

const getPeopleByEmail = (email) => {
  const person = {
    name: '',
    email: '',
    avg_ticket: 0,
    quantity_orders: 0
  };
  let total_gasto=0;
  // list of all Ecommerces
  Object.keys(allData).map((ecommerce) => {
    const translateName = translateKey(ecommerce, "name");
    const translatePrice = translateKey(ecommerce, "price");
    const translateEmail = translateKey(ecommerce, "email");
    const translatePaymentStatus = translateKey(ecommerce, "payment_status");
    const translateStatus_payed = translateKey(ecommerce, "status_payed");
    allData[ecommerce].map((data) => {
      const posName = translateName.split(".");
      const postEmail = translateEmail.split(".");
      const posValor = translatePrice.split(".");

      let name = "";
      let findEmail = "";
      let valor = "";
      

      for (let index = 0; index < postEmail.length; index++) {
        if (!findEmail) {
          findEmail = data[postEmail[index]];
        } else {
          findEmail = findEmail[postEmail[index]];
        }
      }
      if (data[translatePaymentStatus] === translateStatus_payed && email===findEmail) {
        for (let index = 0; index < posName.length; index++) {
          if (!name) {
            name = data[posName[index]];
          } else {
            name = name[posName[index]];
          }
        }    
        for (let index = 0; index < posValor.length; index++) {
          if (!valor) {
            valor = data[posValor[index]];
          } else {
            valor = valor[posValor[index]];
          }
        }
        person.quantity_orders ++;
        total_gasto+=parseFloat(valor)
        person.name = name;
        person.email = email;
      }
    });
  });
  person.avg_ticket=(total_gasto / person.quantity_orders)
  return person;
};
const getPeople = (emails) => {
  let people = [];

  emails.map((email) => {
    const data = getPeopleByEmail(email);
    people.push(data)
  });
  return people;
};

const getSales = () => {
  const total_billing = getTotalBiling();
  const emails = getUniqueEmails();
  const people = getPeople(emails);
  const response = {
    total_billing,
    people,
  };
  return response;
}
router.get("/", async (req, res) => {
  const response = getSales();
  res.json(response);
});

router.post("/create", function (req, res) {

  const source =req.body.source;
  const data =req.body.data;

  Object.keys(allData).map((ecommerce) => {

    if(ecommerce===source){
      allData[ecommerce].push(data)
    }
    

  });
  const response = getSales();
  res.json({response});
});

export default router;
